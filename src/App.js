import React from "react";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import Main from "./Main";

const queryClient = new QueryClient({
  defaultOptions: {
    queries: {
      refetchOnWindowFocus: false,
      staleTime: 1000 * 60,
      retry: false,
    },
  },
});
const App = () => (
  <QueryClientProvider client={queryClient}>
    <Main />
  </QueryClientProvider>
);

export default App;
