import React from "react";
import styled from "styled-components/macro";
import ControlLine from "./components/ControlLine";
import { useMutation } from "@tanstack/react-query";
import DOMPurify from "dompurify";
import { askQuestion } from "./api";

const Main = () => {
  const { data, mutate, isLoading, error, status } = useMutation({
    mutationFn: askQuestion,
  });
  const responseComponents = data?.data?.data || [];
  const isNoResults =
    status !== "idle" && status !== "loading" && !responseComponents.length;

  const handleSearch = async (value) => {
    mutate(value);
  };

  return (
    <Container>
      <ControlLine onSearch={handleSearch} isLoading={isLoading} />

      {error ? (
        <h4>Some error occurred! Please try again later.</h4>
      ) : isNoResults ? (
        <h4>Sorry, no results for your request!</h4>
      ) : (
        responseComponents.map((component, index) => (
          <div
            key={index}
            dangerouslySetInnerHTML={{ __html: DOMPurify.sanitize(component) }}
          />
        ))
      )}
    </Container>
  );
};

const Container = styled.div`
  display: flex;
  flex-direction: column;
  gap: 40px;
  padding: 40px;
  align-items: center;
`;

export default Main;
